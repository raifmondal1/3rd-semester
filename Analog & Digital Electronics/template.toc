\contentsline {chapter}{\numberline {1}Unit 1}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Different Classes of Amplifiers}{2}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Class A Amplifiers}{2}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Class-B Amplifiers}{2}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}Class-AB Amplifiers}{2}{subsection.1.1.3}%
\contentsline {subsection}{\numberline {1.1.4}Class-C Amplifiers}{3}{subsection.1.1.4}%
\contentsline {section}{\numberline {1.2}Recapitulation of Feedback and Oscillation Basics}{3}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Basic Concepts of Feedback}{3}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Oscillation}{3}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Phase Shift in Oscillators}{3}{subsection.1.2.3}%
\contentsline {section}{\numberline {1.3}Learning about Oscillator Circuits and Timing Components}{3}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Wein Bridge Oscillators}{3}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Astable Multivibrators}{4}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Monostable Multivibrators}{4}{subsection.1.3.3}%
\contentsline {subsection}{\numberline {1.3.4}Schmitt Triggers}{4}{subsection.1.3.4}%
\contentsline {subsection}{\numberline {1.3.5}555 Timer}{4}{subsection.1.3.5}%
\contentsline {chapter}{\numberline {2}Unit 2}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Recapitulation of Binary Number System and Boolean Algebra}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Binary Number System}{5}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Boolean Algebra}{5}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Relationship between Binary and Boolean}{5}{subsection.2.1.3}%
\contentsline {section}{\numberline {2.2}Recapitulation of BCD, ASCII, EBCDIC, Gray Codes, and Conversions}{6}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Binary Coded Decimal (BCD)}{6}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}ASCII (American Standard Code for Information Interchange)}{6}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}EBCDIC (Extended Binary Coded Decimal Interchange Code)}{6}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Gray Codes}{6}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Conversions}{6}{subsection.2.2.5}%
\contentsline {section}{\numberline {2.3}Recapitulation of Signed Binary Representation, Binary Arithmetic, Venn Diagrams, and Boolean Algebra}{6}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Signed Binary Number Representation}{6}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Binary Arithmetic}{7}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Venn Diagrams}{7}{subsection.2.3.3}%
\contentsline {section}{\numberline {2.4}Understanding Representation and Combinational Circuits}{7}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Representation in SOP and POS Forms}{7}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Minimization of Logic Expressions}{7}{subsection.2.4.2}%
\contentsline {subsection}{\numberline {2.4.3}Combinational Circuits - Adder and Subtractor}{7}{subsection.2.4.3}%
\contentsline {section}{\numberline {2.5}Recapitulation of Digital Logic Components}{7}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}Encoder}{7}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Decoder}{8}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}Comparator}{8}{subsection.2.5.3}%
\contentsline {subsection}{\numberline {2.5.4}Multiplexer}{8}{subsection.2.5.4}%
\contentsline {subsection}{\numberline {2.5.5}De-Multiplexer}{8}{subsection.2.5.5}%
\contentsline {subsection}{\numberline {2.5.6}Parity Generator}{8}{subsection.2.5.6}%
\contentsline {chapter}{\numberline {3}Unit 3}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Flip-Flops}{9}{section.3.1}%
\contentsline {section}{\numberline {3.2}Registers}{9}{section.3.2}%
\contentsline {section}{\numberline {3.3}Counters}{9}{section.3.3}%
\contentsline {chapter}{\numberline {4}Unit 4}{10}{chapter.4}%
\contentsline {section}{\numberline {4.1}Learning about A/D and D/A Conversion Techniques, and Logic Families}{10}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}D/A Conversion using R-2-R Ladder}{10}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}A/D Conversion using Successive Approximation}{10}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}TTL Logic Family}{10}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}ECL Logic Family}{11}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}MOS and CMOS Logic}{11}{subsection.4.1.5}%
\contentsfinish 
